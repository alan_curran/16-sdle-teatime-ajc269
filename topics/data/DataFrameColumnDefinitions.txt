1 - RowKey: This is a combination of the sample number (ex: 'sa12345.00') and the Step (ex: 1, 2), combined and separated with a hyphen ('-').  The purpose of this column is to identify unique sample-step combinations, to place corresponding measurements between the three measurement types on the same row if they were taken on the same sample at the same step along the exposures.

2 - Sample: This is the standard SDLE 5+2 digit sample id (ex: 'sa12345.00')

3 - Material: This is the type of material (Levels: )

4 - Thick: Sample thickness in centimeter  

5 - Exposure: Which exposure types this particular sample was exposed to (Baseline, DampHeat, FreezeThaw, HotQUV, CyclicQUV)

6 - TotCycle: Total time in one cycle of exposure in minutes, i.e. one cycle in . Exposures are either non-cyclic (all constant conditions) or cyclic with two states. 

7 - S1Cycle: Time spent in cyclic state 1. 

8 - S1T: Temperature in cyclic state 1. 

9 - S1TRate: 

