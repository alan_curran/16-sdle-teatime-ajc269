---
title: "CWRU DSCI351-451: EDA: Rmd Template"
author: "Prof.:Roger French, TA:Yang Hu"
date: "January 13, 2015"
output:
  html_document:
    toc: true
    font-size: 10em
    self_contained: true
  beamer_presentation:
    toc: true
  pdf_document:
    toc: true
---

<!--
# Script Name: 1501cwru-dsci-NAME-IT.Rmd
# Purpose: This is a blank Rmd file to start a new open science data analysis from
# Authors: Roger H. French
# License: Creative Commons Attribution-ShareAlike 4.0 International License.
##########
# Latest Changelog Entires:
# v0.00.01 - 1501cwru-dsci-NAMEIT.RMD - Roger French started this blank Rmd
##########

# Rmd code goes below the comment marker!
-->

## Rmd for Exploratory Data Analysis
 
- EDA is a foundation of Data Science
- Identify sources of data for your problem
- Need to acquire, assemble, clean, and explore your data
- An environment for Exploratory Data Analysis (EDA)  

## R markdown is tool for Open Science
- Reproducible data analysis
- Incorporating Data, Code, Presentation and Reporting
- Good coding practices are essential
- Comment your code, describe your data frames
- Make your data analyses a presentation and report.
 
### Rcode in Rmd, delineated by three ticks{r}

```{r,echo=TRUE,message=TRUE}
options("digits"=5)
options("digits.secs"=3)
```
 
### Code in Rmd is delinearted 
- three ticks for code blocks
- one tick for inline code
 
## YAML settings and commenting
- This is the top block of the Rmd file
- set off by three dashes ---  

### In the YAML Header, you can comment lines with ' ## ' 
### But in the body, ' ## ' is a second level header!

## Commenting in the Rmd body
- To comment in the Rmd body, use html comment form
  < ! - - Comment - - >  
  - but with no spaces between the characters
<!-- regular html comment --> 
''' 
### i.e.' <!-- ' starts a comment block  
### ''' --> ''' ends a comment block  

<!--
  So this is a comment block  
  
   In the YAML header its ##    (a bit confusing since this is a 2nd level header in the rest of the file)
         In the rest of the file its <!-- regular html comment -->
-->

## Inserting Figs, 2 ways.

  ![Caption](./figs/energy_cradle_v5-logo.png)
  
  - <center><img src="./figs/energy_cradle_v5-logo.png" height="150px" width="200px" />

  >- Essential to use relative file paths
  >- Essential to use Posix compatible paths

## Filenames and Paths  
### Filenames
- No Spaces
- No characters other than letters, numbers, - and underscore
- Better not to capitalize
- Or if you must, use CamelBacking

### Paths
- Windows is not Posix compatible
- \ is not understood, must be typed \\
- but should be /, 
>- / always works on Linux, Mac, Windows
- . ie dot, is the current folder
- .. ie dot dot is the folder one above your current area
- setwd (setting working directory) is bad to rely on.  


## Links
 
http://www.r-project.org 

http://rmarkdown.rstudio.com/  

<!--
# Keep a complete change log history at bottom of file.
# Complete Change Log History
# v0.00.00 - 1405-07 - Nick Wheeler made the blank script
##########
Here are standard YAML Headers for Rmarkdown/Markdown/PanDoc
  1. Beamer slides and PDF Report, html
  2. html and PDF report as full long pages
  3.ioSlides slides and PDF report. ioSlides is buggy, not well supported, but looks nice. 

---
title: "Test Presentation"
author: Nick Wheeler
date: January 19, 2015
output:
  html_document:
    toc: true
    font-size: 10em
    self_contained: true
  ioslides_presentation:
    toc: true
    self_contained: true
    smaller: true
  beamer_presentation:
    toc: true
  pdf_document:
    toc: true
---

-->